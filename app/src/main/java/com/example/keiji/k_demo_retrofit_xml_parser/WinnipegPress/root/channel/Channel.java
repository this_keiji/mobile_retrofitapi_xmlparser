package com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.channel;

/**
 * Created by keiji on 23/10/17.
 */

import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.channel.item.Item;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Channel is the main element inside the wpgpress feed
 */
@Root(name = "channel", strict = false)
public class Channel implements Serializable {

    // ELEMENTS
    // elements from the xml (inside the feed element)
    // use @Element to name an element
    @Element(name = "title")
    private String title;

    @Element(name = "description")
    private String description;

    @Element(name = "lastBuildDate")
    private String lastBuildDate;

    @ElementList(inline = true, name = "item")
    private List<Item> items;


    // CONSTRUCTOR
    // because Entry class will be used as a LIST, we need a constructor

    /**
     * DEFAULT constructor
     */
    public Channel () {

    }

    /**
     * Overload constructor with property setters
     * @param title
     * @param description
     * @param lastBuildDate
     * @param items
     */
    public Channel(String title, String description, String lastBuildDate, List<Item> items) {
        this.title = title;
        this.description = description;
        this.lastBuildDate = lastBuildDate;
        this.items = items;
    }


    // Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", lastBuildDate='" + lastBuildDate + '\'' +
                ", items=" + items +
                '}';
    }
}
