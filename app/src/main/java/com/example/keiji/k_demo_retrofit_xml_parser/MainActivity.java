package com.example.keiji.k_demo_retrofit_xml_parser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.WinnipegPressInterface;
import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.WinnipegFeed;
import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.channel.item.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends AppCompatActivity {

    // CONSTANTS
    private static final String TAG = "==>Keiji";
    // feed URL
    private static final String WPG_BASE_URL = "https://www.winnipegfreepress.com/rss/";

    // VARS
    // feed items container
    private List<Item> items;


    /**
     * CONSTRUCTOR
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }//--end of oncreate METHOD


    /**
     * Parse XML
     * @param view
     */
    public void myParseXML (View view) {

        // instanciate retrofit API
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WPG_BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        // intanciate interface and call feed
        final WinnipegPressInterface wpgFeedInterface = retrofit.create(WinnipegPressInterface.class);

        // call wpgfeed using interface getter
        Call<WinnipegFeed> call = wpgFeedInterface.getFeed();

        // using queue to enqueue calls
        // use callback as anonymous function
        call.enqueue(new Callback<WinnipegFeed>() {
            @Override
            public void onResponse(Call<WinnipegFeed> call, Response<WinnipegFeed> response) {

                // verify server response (http)
                Log.d(TAG, "onResponse: server response: " + response.toString());

                // get feed
                Log.d(TAG, "onResponse: FEED: " + response.body().toString());

            }

            @Override
            public void onFailure(Call<WinnipegFeed> call, Throwable t) {

                // log failure
                Log.e(TAG, "onFalure: Unable to get RSS " + t.getMessage());

            }

        });//--end enqueue anonymous FUNCTION

    }//--end of myParseXML METHOD

}//--end of MAIN ACTIVITY
