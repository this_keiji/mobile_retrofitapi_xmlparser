package com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root;

import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.channel.Channel;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by keiji on 23/10/17.
 */

// @Root tells that Feed object is the root | strict=false means we are skipping elements
// obs: all element objects will need to implement serializeble
@Root(name = "rss", strict = false)
public class WinnipegFeed implements Serializable {

    // elements from the xml
    // use @Element to name an element
    @ElementList(inline = true, name = "channel")
    private List<Channel> channel;

    // getters and setters
    // OBS: since there is only 1 channel tag, use
    // .getChannel().get(0) to get the main element
    public List<Channel> getChannel() {
        return channel;
    }

    public void setChannel(List<Channel> channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "Feed: [ Channel: " + channel + " ]";
    }

}//--end of CLASS
