package com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.channel.item;

/**
 * Created by keiji on 23/10/17.
 */

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Item is a child element inside the channel
 */
@Root(name = "item", strict = false)
public class Item implements Serializable {

    // elements
    // elements from the xml (inside the feed element)
    // use @Element to name an element

    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    @Element(name = "description")
    private String description;

    @Element(name = "pubDate")
    private String pubDate;



    // constructor
    // because Entry class will be used as a LIST, we need a constructor

    /**
     * DEFAULT CONSTRUCTOR
     */
    public Item() {

    }

    /**
     * Overloaded constructor with property setters
     * @param title
     * @param link
     * @param description
     * @param pubDate
     */
    public Item(String title, String link, String description, String pubDate) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
    }



    // Getters and Setters

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    @Override
    public String toString() {
        return "\n\nItem { " +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", pubDate='" + pubDate + '\'' +
                " }";
    }

}//--END CLASS
