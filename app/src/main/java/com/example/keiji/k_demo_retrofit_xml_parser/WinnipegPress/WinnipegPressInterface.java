package com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress;

import com.example.keiji.k_demo_retrofit_xml_parser.WinnipegPress.root.WinnipegFeed;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by keiji on 23/10/17.
 */

public interface WinnipegPressInterface {

    // use retrofit get to get the mutable path of url
    @GET("?path=%2Fbreakingnews")
    // use retrofit call method to call wpgFeed and assighn getFeed to it
    Call<WinnipegFeed> getFeed();

}
