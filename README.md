# README #

Demo code of the implementation of the Retrofit simpleXML converter API

### What is this repository for? ###

* This demo shows a basic implementation of the XML converter using the winnipegpress RSS feed

### How do I get set up? ###

* Download or clone the repository and open the project with Android Studio
* run an emulator with Android Lolipop

### References ###

* official page: https://square.github.io/retrofit/ 
* Retrofit simple XML converter github: https://github.com/square/retrofit/tree/master/retrofit-converters/simplexml